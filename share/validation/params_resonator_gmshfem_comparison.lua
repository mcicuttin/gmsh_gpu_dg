-- Resonant cavity test. See Pozar, section 6.3 for theory.

-----------------------
--[[ Problem setup ]]--
sim.name = "test_maxwell_resonator"             -- simulation name
sim.dt = 1e-11                                  -- timestep size
sim.timesteps = 11                           -- num of iterations
sim.gmsh_model = "resonator.geo"                -- gmsh model filename
sim.use_gpu = 0                                 -- 0: cpu, 1: gpu
sim.approx_order = 2                            -- approximation order
sim.time_integrator = "leapfrog"
postpro.silo_output_rate = 100
postpro.cycle_print_rate = 10                  -- console print rate

postpro["E"].silo_mode = "zonal"
postpro["H"].silo_mode = "none"
postpro["J"].silo_mode = "none"

local epsr = 1
local mur = 1

materials[1] = {}
materials[1].epsilon = epsr
materials[1].mu = mur
materials[1].sigma = 0

function electric_initial_condition(x, y, z)
    local Ex = 0
    local Ey = math.sin(math.pi*x) * math.sin(math.pi*z)
    local Ez = 0
    return Ex, Ey, Ez
end

--------------------------
--[[ Validation stuff ]]--
debug = {}

-- determine if we should do I/O
local do_IO = (not parallel) or (parallel and parallel.comm_rank == 0)

local c0 = 1/math.sqrt(const.eps0*const.mu0)
-- Mode
local m = 1     -- along x
local n = 0     -- along y
local l = 1     -- along z
-- Cavity dimensions (must match sim.gmsh_model)
local a = 1     -- along x
local b = 0.1   -- along y
local d = 1     -- along z

local u = m*math.pi/a
local v = n*math.pi/b
local w = l*math.pi/d
-- Compute resonant frequency
local omega = c0*math.sqrt(u*u + v*v + w*w)/math.sqrt(epsr*mur)
local resonance_f = omega/(2*math.pi)
local resonance_MHz = resonance_f/1e6
local cycle_timesteps = 1/(resonance_f*sim.dt)
-- Compute impedance
local eps = materials[1].epsilon * const.eps0
local mu = materials[1].mu * const.mu0
local k_sq = (omega*mu)*(omega*eps)
local kc_sq = u*u + v*v
local beta = math.sqrt(k_sq - kc_sq)
local Zte = omega*mu/beta 
local We = eps*a*b*d/16

if ( do_IO ) then
    print("\x1b[1mANALYTICAL SOLUTION DATA:")
    print("  Resonance frequency: " .. resonance_MHz .. " Mhz")
    print("  Cavity impedance: " .. Zte .. " Ohm")
    print("  Timesteps for 1 cycle: " .. cycle_timesteps)
    print("  Expected energy " .. 2*We .. " J \x1b[0m")
end

function ansol(tag, x, y, z, t)
    local Ex = 0.0
    local Ey = math.cos(omega*t)*math.sin(math.pi*x)*math.sin(math.pi*z)
    local Ez = 0.0
    local Hx = math.sin(omega*t)*math.sin(math.pi*x)*math.cos(math.pi*z)/Zte
    local Hy = 0.0
    local Hz = -math.sin(omega*t)*math.cos(math.pi*x)*math.sin(math.pi*z)/Zte

    return Ex, Ey, Ez, Hx, Hy, Hz
end

debug.analytical_solution = ansol
--debug.dump_cell_ranks = true

function on_timestepping_finished()
    err = compute_error()
    if ( do_IO ) then
        print("\x1b[32m*** GMSH-FEM COMPARISON DATA ***\x1b[0m")
        print("Error on Ey: " .. err.Ey)
        compare_at_gauss_points() -- NOT MPI SAFE
    end
end
