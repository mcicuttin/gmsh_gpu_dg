-- Validation test: simulate a plane wave propagating in a parallel plate
-- waveguide terminated with an absorbing boundary condition.

sim.name = "test_maxwell_planewave_absorbing"   -- simulation name
sim.dt = 1e-11                                  -- timestep size
sim.timesteps = 100                             -- num of iterations
sim.gmsh_model = "parallel_plate.geo"           -- gmsh model filename
sim.use_gpu = 0                                 -- 0: cpu, 1: gpu
sim.approx_order = 1                            -- approximation order
sim.time_integrator = "leapfrog"
postpro.silo_output_rate = 100
postpro.cycle_print_rate = 100                  -- console print rate

postpro["E"].mode = "nodal"

materials[1] = {}
materials[1].epsilon = 1
materials[1].mu = 1
materials[1].sigma = 0

-- ** Boundary conditions **
local PMC_bnds = { 3, 4 }
for i,v in ipairs(PMC_bnds) do
    bndconds[v] = {}
    bndconds[v].kind = "pmc"
end

bndconds[6] = {}
bndconds[6].kind = "impedance"

local freq = 3e8
function source(tag, x, y, z, t)
    local Ex = math.sin(2*math.pi*t*freq)
    local Ey = 0
    local Ez = 0
    return Ex, Ey, Ez
end

bndconds[5] = {}
bndconds[5].kind = "plane_wave_E"
bndconds[5].source = source

debug = {}

local eps = materials[1].epsilon * const.eps0
local mu = materials[1].mu * const.mu0
local Z = math.sqrt(mu/eps)

function ansol(tag, x, y, z, t)
    local Ex = math.sin(2*math.pi*z - 2*math.pi*t*freq)
    local Ey = 0.0
    local Ez = 0.0
    local Hx = 0.0
    local Hy = 0.0
    local Hz = 0.0

    return Ex, Ey, Ez, Hx, Hy, Hz
end

debug.analytical_solution = ansol
