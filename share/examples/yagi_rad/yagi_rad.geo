SetFactory("OpenCASCADE");

Mesh.MeshSizeFromCurvature = 5;
Mesh.MeshSizeMax=0.4;

spradius = 3;
MHz = 300;

ER = 0.005;

R = 0.5*(300/MHz);
A = 0.44*(300/MHz);
D = 0.43*(300/MHz);
sp_RA = 0.25*(300/MHz);
sp_AD = 0.13*(300/MHz);

src = A/20;
Ax = (A-src)/2;

Sphere(1) = {0, 0, 0, spradius, -Pi/2, Pi/2, 2*Pi};
Cylinder(2) = {-sp_RA, 0, -R/2, 0, 0, R, ER, 2*Pi};
Cylinder(3) = {0, 0, src/2, 0, 0, Ax, ER, 2*Pi};
Cylinder(4) = {0, 0, -src/2, 0, 0, src, ER, 2*Pi};
Cylinder(5) = {0, 0, src/2-Ax, 0, 0, Ax, ER, 2*Pi};
Cylinder(6) = {sp_AD, 0, -D/2, 0, 0, D, ER, 2*Pi};
Coherence;

//MeshSize { Volume{2}  } = 0.01;
//MeshSize {15,16,17,18,19,20,21,22} = 0.1;
//+
