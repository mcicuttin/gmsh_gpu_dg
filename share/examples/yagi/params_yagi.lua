--[[
    Model of a 3-element yagi antenna receiving a plane wave at 300 MHz
]]--
sim.name = "yagi"              -- simulation name
sim.dt = 1e-12                      -- timestep size
sim.timesteps = 20000               -- num of iterations
sim.gmsh_model = "yagi.geo"    -- gmsh model filename
sim.use_gpu = 0                     -- 0: cpu, 1: gpu
sim.approx_order = 1                -- approximation order
sim.time_integrator = "leapfrog"
postpro.silo_output_rate = 100       -- rate at which to write silo files
postpro.cycle_print_rate = 10       -- console print rate

-- ** Materials **
-- Aluminum
local alu = {2, 3, 4}
for i,v in ipairs(alu) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 3.69e7 
end

local air = {5}
for i,v in ipairs(air) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 0
end

-- ** Boundary conditions **
local Z_bnds = {16}
for i,v in ipairs(Z_bnds) do
    bndconds[v] = {}
    bndconds[v].kind = "impedance"
end

local pmc_bnds = {17, 19}
for i,v in ipairs(pmc_bnds) do
    bndconds[v] = {}
    bndconds[v].kind = "pmc"
end

local freq = 3e8
function interface_source(tag, x, y, z, t)
    local Ex = 0
    local Ey = 0
    local Ez = math.sin(2*math.pi*t*freq)
    return Ex, Ey, Ez
end

bndconds[21] = {}
bndconds[21].kind = "plane_wave_E"
bndconds[21].source = interface_source


