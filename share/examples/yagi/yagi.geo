SetFactory("OpenCASCADE");

MHz = 300;

R = 0.5*(300/MHz);
A = 0.44*(300/MHz);
D = 0.43*(300/MHz);
sp_RA = 0.25*(300/MHz);
sp_AD = 0.13*(300/MHz);

Box(1) = {0, 0, 0, 1, 1, 1};
Cylinder(2) = {0.5-sp_RA, 0.5, 0.5-R/2, 0, 0, R, 0.01, 2*Pi};
Cylinder(3) = {0.5, 0.5, 0.5-A/2, 0, 0, A, 0.01, 2*Pi};
Cylinder(4) = {0.5+sp_AD, 0.5, 0.5-D/2, 0, 0, D, 0.01, 2*Pi};
Coherence;

MeshSize {9,10,11,12,13,14} = 0.01;
//MeshSize {15,16,17,18,19,20,21,22} = 0.1;
