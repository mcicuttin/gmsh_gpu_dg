SetFactory("OpenCASCADE");

width = 0.30;
height = 0.20;
depth = 0.28;

wg_width = 0.10;
wg_height = 0.05;

Box(1) = {0.0, 0.0, 0.0, width, height, depth};

Rectangle(100) = { width/2 - wg_width/2, height/2 - wg_height/2, 0 , wg_width, wg_height, 0 };

BooleanFragments{ Volume{1}; Delete; }{ Surface{100}; Delete; }

MeshSize{:} = 0.02;
