sim.name = "oven"                       -- simulation name
sim.dt = 1e-13                          -- timestep size
sim.timesteps = 100000                   -- num of iterations
sim.gmsh_model = "oven.geo"            -- gmsh model filename
sim.use_gpu = 0                         -- 0: cpu, 1: gpu
sim.approx_order = 1                    -- approximation order
sim.time_integrator = "leapfrog"
postpro.silo_output_rate = 100           -- rate at which to write silo files
postpro.cycle_print_rate = 100           -- console print rate

postpro["H"].silo_mode = "none"
postpro["J"].silo_mode = "none"

debug = {}
debug.dump_cell_ranks = true

materials[1] = {}
materials[1].epsilon = 1
materials[1].mu = 1
materials[1].sigma = 0

-- ** Boundary conditions **
local freq = 2.45e9
function source(tag, x, y, z, t)
    local Ex = 0
    local Ey = math.sin((x-0.1)*math.pi/0.1)*math.sin(2*math.pi*t*freq)
    local Ez = 0
    return Ex, Ey, Ez
end

bndconds[100] = {}
bndconds[100].kind = "plane_wave_E"
bndconds[100].source = source


