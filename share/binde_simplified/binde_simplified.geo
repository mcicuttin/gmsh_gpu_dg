SetFactory("OpenCASCADE");
Box(1) = {0, 0, 0, 3, 3, 3};
Box(2) = {1+0.3, 1+0.45, 1+0.45, 0.4, 0.1, 0.1};
Coherence;

// Air box
MeshSize {17, 18, 19, 20, 21, 22, 23, 24} = 0.2;

// Metal block
MeshSize {9, 10, 11, 12, 13, 14, 15, 16} = 0.05;
