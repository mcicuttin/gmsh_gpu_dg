--[[
    This example is a simplified version of a more complicated model
    of an electrostatic discharge applied to a metallic box.
    The discharge is modeled as a surface current source, but this is
    not physically correct. This model is more a test for interface
    BCs and sources than anything else.
--]]
sim.name = "binde_simplified"           -- simulation name
sim.dt = 1e-11                          -- timestep size
sim.timesteps = 10000                   -- num of iterations
sim.gmsh_model = "binde_simplified.geo" -- gmsh model filename
sim.use_gpu = 0                         -- 0: cpu, 1: gpu
sim.approx_order = 1                    -- approximation order
--sim_geom_order = 1                    -- geometric order, only 1 for now
postpro.silo_output_rate = 10           -- rate at which to write silo files
postpro.cycle_print_rate = 10           -- console print rate

sim.time_integrator = "leapfrog"        -- use leapfrog with dissipative materials

-- ** Materials **
-- Aluminum
local alu = {2}
for i,v in ipairs(alu) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 1
end

local air = {3}
for i,v in ipairs(air) do
    materials[v] = {}
    materials[v].epsilon = 1
    materials[v].mu = 1
    materials[v].sigma = 0
end

-- ** Boundary conditions **
local Z_bnds = {13, 14, 15, 16, 17, 18}
for i,v in ipairs(Z_bnds) do
    bndconds[v] = {}
    bndconds[v].kind = "impedance"
end

-- Linear interpolation
function interp(t, t0, t1, y0, y1)
    return (t-t0)*(y1-y0)/(t1-t0) + y0
end

-- Shape of the electrostatic discharge given as time-value pairs
local current_shape = { {0, 0}, {9.5e-9, 0.03}, {1.1e-8, 1}, 
                        {1.4e-8, 0.45}, {1.6e-8, 0.45}, {2.7e-8, 0.6},
                        {9e-8, 0.01}, {1e-7, 0} }

-- Function that evaluates the source
function interface_source_7(tag, x, y, z, t)
    for ii = 1,(#current_shape-1) do
        local t0 = current_shape[ii][1]
        local c0 = current_shape[ii][2]
        local t1 = current_shape[ii+1][1]
        local c1 = current_shape[ii+1][2]
        if (t >= t0 and t < t1) then
            Ez = interp(t, t0, t1, c0, c1)
            return 0, 0, Ez
        end
    end
    return 0, 0, 0
end

-- Surface current interface condition
local discharge_surf = 7
ifaceconds[discharge_surf] = {}
ifaceconds[discharge_surf].kind = "surface_current"
ifaceconds[discharge_surf].source = interface_source_7


