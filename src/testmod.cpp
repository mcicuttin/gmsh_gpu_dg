#include <iostream>

#ifdef USE_MPI
#include <mpi/mpi.h>
#endif

#include "gmsh_io.h"

std::pair<int, int>
priv_MPI_Procinfo()
{
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    return std::make_pair(rank, size);
}

void
init_model_rank0(model& mod)
{
    std::cout << "Rank 0 init" << std::endl;

    auto [comm_rank, comm_size] = priv_MPI_Procinfo();

    mod.generate_mesh();
    if (comm_size > 1)
        mod.partition_mesh(comm_size);

    mod.populate_from_gmsh();

}

void
init_model_rankN(model& mod)
{
    std::cout << "Rank N init" << std::endl;
    mod.populate_from_gmsh();
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Please specify .geo" << std::endl;
        return 1;
    }

    MPI_Init(&argc, &argv);

    auto [rank, size] = priv_MPI_Procinfo();

    model mod;

    if (rank == 0)
    {
        gmsh::initialize();
        gmsh::open(argv[1]);
        init_model_rank0(mod);
    }
    else
    {
        init_model_rankN(mod);
    }



    if (rank == 0)
    {
        gmsh::finalize();
    }

    MPI_Finalize();
    return 0;
}