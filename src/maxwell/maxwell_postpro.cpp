
/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <fstream>
#include <fcntl.h>

#ifdef USE_MPI
#include "common/mpi_helpers.h"
#endif /* USE_MPI */

#include "maxwell/maxwell_postpro.h"

namespace maxwell {

field_values
eval_field(const field& f, size_t ofs, size_t cbs, const vecxd& phi)
{
    assert(ofs+cbs <= f.num_dofs);
    field_values ret;
    ret.Ex = f.Ex.segment(ofs, cbs).dot(phi);
    ret.Ey = f.Ey.segment(ofs, cbs).dot(phi);
    ret.Ez = f.Ez.segment(ofs, cbs).dot(phi);
    ret.Hx = f.Hx.segment(ofs, cbs).dot(phi);
    ret.Hy = f.Hy.segment(ofs, cbs).dot(phi);
    ret.Hz = f.Hz.segment(ofs, cbs).dot(phi);
    return ret;
}

field_values
compute_error(const model& mod, const solver_state& state, const parameter_loader& mpl)
{
    field_values err;

    if (not mpl.has_analytical_solution())
        throw std::invalid_argument("analytical solution not defined in the Lua configuration");

    for (auto& e : mod)
    {
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            const auto& pe = e.cell(iT);
            const auto& re = e.cell_refelem(pe);

            const auto pqps = pe.integration_points();

            const auto ofs = e.cell_model_dof_offset(iT);
            const auto cbs = re.num_basis_functions();

            for (size_t iQp = 0; iQp < pqps.size(); iQp++)
            {
                const auto& pqp = pqps[iQp];
                const vecxd phi = re.basis_functions(iQp);

                field_values Fh = eval_field(state.emf_curr, ofs, cbs, phi);

                auto [E, H] = mpl.eval_analytical_solution(e.material_tag(),
                    pqp.point(), state.curr_time);
                field_values F(E,H);
                field_values Fdiff = F - Fh;
                err += Fdiff*Fdiff*pqp.weight();
            }
        }
    }

#ifdef USE_MPI
    MPI_Allreduce(MPI_IN_PLACE, &err, 6, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif /* USE_MPI */
    return sqrt(err);
}

field_values
compute_energy(const model& mod, const solver_state& state, const parameter_loader& mpl)
{
    field_values energy;

    for (auto& e : mod)
    {
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            const auto& pe = e.cell(iT);
            const auto& re = e.cell_refelem(pe);

            const auto pqps = pe.integration_points();

            const auto ofs = e.cell_model_dof_offset(iT);
            const auto cbs = re.num_basis_functions();

            const auto bar = pe.barycenter();
            auto eps = mpl.epsilon( e.material_tag(), bar );
            auto mu = mpl.mu( e.material_tag(), bar );

            field_values mat(eps, eps, eps, mu, mu, mu);

            for (size_t iQp = 0; iQp < pqps.size(); iQp++)
            {
                const auto& pqp = pqps[iQp];
                const vecxd phi = re.basis_functions(iQp);
                field_values Fh = eval_field(state.emf_curr, ofs, cbs, phi);
                energy += 0.5*mat*Fh*Fh*pqp.weight();
            }
        }
    }

#ifdef USE_MPI
    MPI_Allreduce(MPI_IN_PLACE, &energy, 6, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif /* USE_MPI */
    return energy;
}

#ifdef ENABLE_GPU_SOLVER
static void
validate(const model&, maxwell::solver_state_gpu&,
    maxwell::parameter_loader&, std::ofstream&)
{}
#endif /* ENABLE_GPU_SOLVER */


void
compare_at_gauss_points(const model& mod, const solver_state& state, const parameter_loader& mpl)
{
    struct qpFval { double Fx, Fy, Fz; };

    size_t point_id = 0;
    std::ofstream pmap_ofs("pointmap.txt");

    double errorsq_dg_vs_gmshfem = 0.0;
    double errorsq_dg_vs_ana = 0.0;
    double errorsq_gmshfem_vs_ana = 0.0;
    double normsq_dg = 0.0;
    double normsq_gmshfem = 0.0;
    double normsq_ana = 0.0;
    for (auto& e : mod)
    {
        std::stringstream ss;
        ss << "e_" << e.gmsh_dim() << "_" << e.gmsh_tag() << ".dat";

        std::cout << "Loading " << ss.str() << std::endl;
        int fd = open(ss.str().c_str(), O_RDONLY);
        if (fd < 0)
        {
            std::cout << "Can't open '" << ss.str() << "'" << std::endl;
            return;
        }
        size_t num_vals;
        read(fd, &num_vals, sizeof(size_t));
        std::cout << "Reading " << num_vals << " values" << std::endl;
        std::vector<qpFval> qpFvals(num_vals);
        read(fd, qpFvals.data(), num_vals*sizeof(qpFval));
        close(fd);

        std::cout << "Num of cells: " << e.num_cells() << std::endl;    
        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            const auto& pe = e.cell(iT);
            const auto& re = e.cell_refelem(pe);
            const auto ofs = e.cell_model_dof_offset(iT);
            const auto cbs = re.num_basis_functions();
            const auto pqps = pe.integration_points();
            
            const auto gmsh_pos = e.cell_local_index_by_gmsh(iT);
            const size_t num_qps = pqps.size();
            const size_t qps_pos_base = num_qps*gmsh_pos;
            for (size_t iQp = 0; iQp < num_qps; iQp++)
            {
                const auto& pqp = pqps[iQp];
                const vecxd phi = re.basis_functions(iQp);
                field_values Fh = eval_field(state.emf_curr, ofs, cbs, phi);
                auto& qpFval = qpFvals.at(qps_pos_base+iQp);
                auto Exd = Fh.Ex - qpFval.Fx;
                auto Eyd = Fh.Ey - qpFval.Fy;
                auto Ezd = Fh.Ez - qpFval.Fz;
                errorsq_dg_vs_gmshfem += pqp.weight() * (Exd*Exd + Eyd*Eyd + Ezd*Ezd);
                normsq_dg += pqp.weight() * (Fh.Ex*Fh.Ex + Fh.Ey*Fh.Ey + Fh.Ez*Fh.Ez);
                normsq_gmshfem += pqp.weight() * (qpFval.Fx*qpFval.Fx + qpFval.Fy*qpFval.Fy + qpFval.Fz*qpFval.Fz);

                auto p = pqp.point();
                auto Ey_ana = std::sin(M_PI*p.x())*std::sin(M_PI*p.z())*std::cos(2.*M_PI*211.98528005809e6*state.curr_time);
                normsq_ana += pqp.weight() * Ey_ana * Ey_ana;
                errorsq_dg_vs_ana += pqp.weight() * ( (Fh.Ey - Ey_ana)*(Fh.Ey - Ey_ana) );
                errorsq_gmshfem_vs_ana += pqp.weight() * ( (qpFval.Fy - Ey_ana)*(qpFval.Fy - Ey_ana) );
                pmap_ofs << pe.element_tag() << " " << qps_pos_base+iQp << " " << p.x() << " " << p.y() << " " << p.z() << " " << point_id++ << std::endl;
            }
        }
    }

    double norm_dg_error = 100.*(std::sqrt(normsq_dg) - std::sqrt(normsq_ana))/std::sqrt(normsq_ana);
    double norm_gmshfem_error = 100.*(std::sqrt(normsq_gmshfem) - std::sqrt(normsq_ana))/std::sqrt(normsq_ana);
    std::cout << sgr::Bredfg << "Errors and norms:" << sgr::reset << std::endl;
    std::cout << " Analytic norm:        " << std::sqrt(normsq_ana) << std::endl;
    std::cout << " Computed norm (DG):   " << std::sqrt(normsq_dg) << ", off by " << norm_dg_error << " %" << std::endl;
    std::cout << sgr::BYellowfg;
    std::cout << " Computed norm (FEM):  " << std::sqrt(normsq_gmshfem) << ", off by " << norm_gmshfem_error << " %" << std::endl;
    std::cout << sgr::reset;

    auto dgvsfem_percent = 100.*std::sqrt(errorsq_dg_vs_gmshfem)/std::sqrt(normsq_gmshfem);
    std::cout << " DG vs. FEM:           " << std::sqrt(errorsq_dg_vs_gmshfem) << ", " << dgvsfem_percent << " %" << std::endl;
    std::cout << " DG vs. analytic:      " << std::sqrt(errorsq_dg_vs_ana) << std::endl;
    std::cout << sgr::BYellowfg;
    std::cout << " FEM vs. analytic:     " << std::sqrt(errorsq_gmshfem_vs_ana) << std::endl;
    std::cout << sgr::reset;
}


} // namespace maxwell

