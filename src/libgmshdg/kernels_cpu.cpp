/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include "common/types.h"
#include "libgmshdg/entity_data.h"
#include "libgmshdg/kernels_cpu.h"

/* Field derivatives kernel, case for elements with planar faces */
void compute_field_derivatives(const entity_data_cpu& ed,
    const vecxd& f, vecxd& df_dx, vecxd& df_dy, vecxd& df_dz)
{
    switch (ed.a_order)
    {
        case 1:
            if (ed.g_order == 1)
                compute_field_derivatives_kernel_planar<1>(ed, f, df_dx, df_dy, df_dz);
            else
                compute_field_derivatives_kernel_curved<1>(ed, f, df_dx, df_dy, df_dz);
            break;

        case 2:
            if (ed.g_order == 1)
                compute_field_derivatives_kernel_planar<2>(ed, f, df_dx, df_dy, df_dz);
            else
                compute_field_derivatives_kernel_curved<2>(ed, f, df_dx, df_dy, df_dz);
            break;

        case 3:
            if (ed.g_order == 1)
                compute_field_derivatives_kernel_planar<3>(ed, f, df_dx, df_dy, df_dz);
            else
                compute_field_derivatives_kernel_curved<3>(ed, f, df_dx, df_dy, df_dz);
            break;

        case 4:
            if (ed.g_order == 1)
                compute_field_derivatives_kernel_planar<4>(ed, f, df_dx, df_dy, df_dz);
            else
                compute_field_derivatives_kernel_curved<4>(ed, f, df_dx, df_dy, df_dz);
            break;
        
        case 5:
            if (ed.g_order == 1)
                compute_field_derivatives_kernel_planar<5>(ed, f, df_dx, df_dy, df_dz);
            else
                compute_field_derivatives_kernel_curved<5>(ed, f, df_dx, df_dy, df_dz);
            break;

        case 6:
            if (ed.g_order == 1)
                compute_field_derivatives_kernel_planar<6>(ed, f, df_dx, df_dy, df_dz);
            else
                compute_field_derivatives_kernel_curved<6>(ed, f, df_dx, df_dy, df_dz);
            break;

        default:
            std::cout << "compute_field_derivatives: invalid order" << std::endl;
    }
}

/* Field derivatives kernel, case for elements with planar faces */
void compute_flux_lifting(const entity_data_cpu& ed,
    const vecxd& flux, vecxd& field)
{
    switch (ed.a_order)
    {
        case 1:
            if (ed.g_order == 1)
                compute_lifting_kernel_planar<1>(ed, flux, field);
            else
                compute_lifting_kernel_curved<1>(ed, flux, field);
            break;

        case 2:
            if (ed.g_order == 1)
                compute_lifting_kernel_planar<2>(ed, flux, field);
            else
                compute_lifting_kernel_curved<2>(ed, flux, field);
            break;

        case 3:
            if (ed.g_order == 1)
                compute_lifting_kernel_planar<3>(ed, flux, field);
            else
                compute_lifting_kernel_curved<3>(ed, flux, field);
            break;

        case 4:
            if (ed.g_order == 1)
                compute_lifting_kernel_planar<4>(ed, flux, field);
            else
                compute_lifting_kernel_curved<4>(ed, flux, field);
            break;
        
        case 5:
            if (ed.g_order == 1)
                compute_lifting_kernel_planar<5>(ed, flux, field);
            else
                compute_lifting_kernel_curved<5>(ed, flux, field);
            break;

        case 6:
            if (ed.g_order == 1)
                compute_lifting_kernel_planar<6>(ed, flux, field);
            else
                compute_lifting_kernel_curved<6>(ed, flux, field);
            break;

        default:
            std::cout << "compute_flux_lifting: invalid order" << std::endl;
    }
}
