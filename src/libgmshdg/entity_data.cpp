/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include "libgmshdg/entity_data.h"

#ifdef ENABLE_GPU_SOLVER
entity_data_gpu::entity_data_gpu()
{
}

entity_data_gpu::entity_data_gpu(const entity_data_cpu& ed)
{
    g_order = ed.g_order;
    a_order = ed.a_order;

    num_all_elems = num_elems_all_orientations(ed);

    std::vector<int32_t> eo;
    eo.reserve(num_all_elems);
    for (int32_t iO = 0; iO < ed.num_orientations; iO++)
    {
        for (size_t iT = 0; iT < ed.num_elems[iO]; iT++)
            eo.push_back(iO);
    }
    assert(eo.size() == num_all_elems);
    element_orientation.copyin(eo.data(), eo.size());

    num_bf = ed.num_bf;
    num_fluxes = ed.num_fluxes;
    dof_base = ed.dof_base;
    flux_base = ed.flux_base;
    num_faces_per_elem = ed.num_faces_per_elem;
    
    auto rows = ed.num_bf*ed.num_orientations;
    auto cols = ed.num_bf*3;
    MatxRM<double> dm = MatxRM<double>::Zero(rows, cols);
    for (size_t i = 0; i < ed.num_orientations; i++)
    {
        dm.block(i*num_bf, 0, num_bf, num_bf) =
            ed.differentiation_matrices.block(i*num_bf, 0, num_bf, num_bf).transpose();
        
        dm.block(i*num_bf, num_bf, num_bf, num_bf) =
            ed.differentiation_matrices.block(i*num_bf, num_bf, num_bf, num_bf).transpose();

        dm.block(i*num_bf, 2*num_bf, num_bf, num_bf) =
            ed.differentiation_matrices.block(i*num_bf, 2*num_bf, num_bf, num_bf).transpose();
    }
    differentiation_matrices.init(dm.data(), dm.size());

    auto LMrows = ed.num_bf;
    auto LMcols = 4*ed.num_fluxes*ed.num_orientations;
    MatxCM<double> lm = MatxCM<double>::Zero(LMrows, LMcols);
    for (size_t iO = 0; iO < ed.num_orientations; iO++)
    {
        /*
        lm.block(0, 4*i*ed.num_fluxes, ed.num_bf, 4*ed.num_fluxes) = 
            ed.lifting_matrices.block(i*ed.num_bf, 0, ed.num_bf, 4*ed.num_fluxes);
        */
        for (size_t i = 0; i < ed.num_bf; i++)
        {
            for (size_t j = 0; j < 4*ed.num_fluxes; j++)
            {
                auto src_row = iO*ed.num_bf+i;
                auto src_col = j;
                auto dst_row = i;
                auto dst_col = iO*4*ed.num_fluxes + j;//( ((4*ed.num_fluxes - i) + j)%(4*ed.num_fluxes) );
                lm(dst_row, dst_col) = ed.lifting_matrices(src_row, src_col);
            }
        }
    }
/*
    matxd orig = ed.lifting_matrices.block(0, 0, ed.num_bf, 4*ed.num_fluxes);
    matxd rotated = lm.block(0, 0, ed.num_bf, 4*ed.num_fluxes);

    std::cout << "*** ORIG ***" << std::endl;
    std::cout << orig << std::endl;
    std::cout << "*** ROTATED ***" << std::endl;
    std::cout << rotated << std::endl;
*/
    lifting_matrices.init(lm.data(), lm.size());

    jacobians.copyin(ed.jacobians.data(), ed.jacobians.size());

    cell_determinants.copyin(ed.cell_determinants.data(), ed.cell_determinants.size());

    fluxdofs_mine.copyin(ed.fluxdofs_mine.data(), ed.fluxdofs_mine.size());
    fluxdofs_neigh.copyin(ed.fluxdofs_neigh.data(), ed.fluxdofs_neigh.size());
    normals.copyin(ed.normals.data(), ed.normals.size());
    face_determinants.copyin(ed.face_determinants.data(), ed.face_determinants.size());
}

entity_data_gpu::~entity_data_gpu()
{
}

#endif /* ENABLE_GPU_SOLVER */
