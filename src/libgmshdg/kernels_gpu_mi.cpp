/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include "libgmshdg/kernels_gpu.h"

/* Return how many dblocks are needed to store this entity on GPU. */
size_t gpu_dblocks(const entity_data_cpu& ed)
{
    auto ks = get_kernel_gpu_sizes(ed.a_order);

    size_t tot_elems = num_elems_all_orientations(ed);
    size_t num_dblocks = tot_elems / ks.cells_per_dblock;
    if (num_dblocks % ks.cells_per_dblock)
        num_dblocks += 1;

    return num_dblocks;
}

/* Return the total number of dofs (actual + padding) that are contained
 * in the dblocks making up this entity. */
size_t gpu_dblocks_dofs(const entity_data_cpu& ed)
{
    auto ks = get_kernel_gpu_sizes(ed.a_order);
    size_t num_dblocks = gpu_dblocks(ed);
    return num_dblocks * ks.dblock_size;
}

void reshape_dofs(const entity_data_cpu& ed, const entity_data_gpu& edg,
                  const vecxd& in_field, vecxd& out_field, bool to_gpu)
{
    auto ks = get_kernel_gpu_sizes(ed.a_order);

    size_t tot_elems = num_elems_all_orientations(ed);
    size_t num_dblocks = tot_elems / ks.cells_per_dblock;
    if (num_dblocks % ks.cells_per_dblock)
        num_dblocks += 1;

    for (size_t i = 0; i < num_dblocks; i++)
    {
        for (size_t j = 0; j < ks.cells_per_dblock; j++)
        {
            auto cur_elem = ks.cells_per_dblock*i + j;
            if (cur_elem >= tot_elems)
                break;
            
            auto cpu_base = ed.dof_base + cur_elem * ed.num_bf;
            auto gpu_base = edg.dof_base + ks.dblock_size*i + ks.num_bf*j;

            if (to_gpu)
                for (size_t k = 0; k < ks.num_bf; k++)
                    out_field(gpu_base + k) = in_field(cpu_base + k);
            else
                for (size_t k = 0; k < ks.num_bf; k++)
                    out_field(cpu_base + k) = in_field(gpu_base + k);
        }
    }
}

