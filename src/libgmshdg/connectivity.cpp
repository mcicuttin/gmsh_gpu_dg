/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <iostream>

#include "libgmshdg/connectivity.h"

/***************************************************************************/
neighbour_descriptor::neighbour_descriptor()
{}

neighbour_descriptor::neighbour_descriptor(size_t p_iE, size_t p_iT, size_t p_via_iF)
    : iE(p_iE), iT(p_iT), via_iF(p_via_iF)
{}

bool
neighbour_descriptor::operator<(const neighbour_descriptor& other) const
{
    std::array<size_t, 3>   arr_me{iE, iT, via_iF};
    std::array<size_t, 3>   arr_other{other.iE, other.iT, other.via_iF};
    return arr_me < arr_other;
}

std::ostream& operator<<(std::ostream& os, const neighbour_descriptor& nd)
{
    os << "(" << nd.iE << ", " << nd.iT << ", " << nd.via_iF << ")";
    return os;
}




neighbour_descriptor_interprocess::neighbour_descriptor_interprocess()
{}

bool
neighbour_descriptor_interprocess::operator<(const neighbour_descriptor_interprocess& other) const
{
    std::array<size_t, 3>   arr_me{iE, iT, via_iF};
    std::array<size_t, 3>   arr_other{other.iE, other.iT, other.via_iF};
    return arr_me < arr_other;
}

std::ostream& operator<<(std::ostream& os, const neighbour_descriptor_interprocess& nd)
{
    os << "(" << nd.iE << ", " << nd.iT << ", " << nd.via_iF << ", PTAG: " << nd.parent_tag;
    os << ", TAG: " << nd.tag << ", PART: " << nd.partition << ")";
    return os;
}

/***************************************************************************/
cell_descriptor::cell_descriptor()
{}

cell_descriptor::cell_descriptor(size_t p_iE, size_t p_iT)
    : iE(p_iE), iT(p_iT)
{}

bool
cell_descriptor::operator<(const cell_descriptor& other) const
{
    if (iE < other.iE)
        return true;

    if (iE == other.iE)
        return (iT < other.iT);

    return false;
}

std::ostream& operator<<(std::ostream& os, const cell_descriptor& cd)
{
    os << "(" << cd.iE << "," << cd.iT << ")"; 
    return os;
}

/***************************************************************************/
