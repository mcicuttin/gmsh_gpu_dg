/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#include <iostream>
#include <unistd.h>

#include "test.h"
#include "sgr.hpp"
#include "libgmshdg/gmsh_io.h"

using namespace sgr;

static void
make_geometry(int order, double mesh_h)
{
    gm::add("test");

    std::vector<std::pair<int,int>> objects;
    objects.push_back(
        std::make_pair(3, gmo::addBox(0.0, 0.0, 0.0, 1.0, 1.0, 1.0) )
        );
    objects.push_back(
        std::make_pair(3, gmo::addSphere(0.5, 0.5, 0.5, SPHERE_RADIUS) )
        );

    std::vector<std::pair<int, int>> tools;
    gmsh::vectorpair odt;
    std::vector<gmsh::vectorpair> odtm;
    gmo::fragment(objects, tools, odt, odtm);

    gmo::synchronize();

    int pgtag = gm::addPhysicalGroup(2, {7});
    gm::setPhysicalName(2, pgtag, "SphereSurface");

    gvp_t vp;
    gm::getEntities(vp);
    gmm::setSize(vp, mesh_h);
}

int
test_basics(int geometric_order, int approximation_order)
{
    make_geometry(0, 0.1);
    gmm::generate( DIMENSION(3) );

    model mod(geometric_order, approximation_order);
    mod.build();

    auto idx = geometric_order - 1;

    std::cout << cyanfg << "Testing geometric order " << geometric_order;
    std::cout << ", approximation order = " << approximation_order << nofg;
    std::cout << std::endl;

    double vol_e0 = (4.0/3.0)*M_PI*SPHERE_RADIUS*SPHERE_RADIUS*SPHERE_RADIUS;
    double vol_e1 = 1.0 - vol_e0;

    /*****************************************/
    std::cout << yellowfg << "Volumes by reference quadrature points and determinants";
    std::cout << nofg << std::endl;
    std::vector<double> t1_vals { 0.07, 0.00145, 0.00024, 1.83e-5 };
    COMPARE_VALUES_RELATIVE("Entity 0 vol r+d", mod.entity_at(0).measure(), vol_e0, t1_vals[idx]);
    std::vector<double> t2_vals { 0.0024, 5.02e-5, 8.21e-06, 6.35e-7 };
    COMPARE_VALUES_RELATIVE("Entity 1 vol r+d", mod.entity_at(1).measure(), vol_e1, t2_vals[idx]);

    /*****************************************/
    std::cout << yellowfg << "Volumes by physical quadrature points" << nofg << std::endl;
    double vol_e0_pqp = 0.0;
    auto& e0 = mod.entity_at(0);
    for (const auto& e : e0)
    {
        const auto& pqps = e.integration_points();
        for (const auto& qp : pqps)
            vol_e0_pqp += qp.weight(); 
    }
    COMPARE_VALUES_RELATIVE("Entity 0 vol pqp", vol_e0_pqp, vol_e0, t1_vals[idx]);

    double vol_e1_pqp = 0.0;
    auto& e1 = mod.entity_at(1);
    for (const auto& e : e1)
    {
        const auto& pqps = e.integration_points();
        for (const auto& qp : pqps)
            vol_e1_pqp += qp.weight(); 
    }
    COMPARE_VALUES_RELATIVE("Entity 1 vol pqp", vol_e1_pqp, vol_e1, t2_vals[idx]);

    /*****************************************/
    std::cout << yellowfg << "Volumes by jacobians" << nofg << std::endl;
    double vol_e0_j = 0.0;
    for (const auto& e : e0)
    {
        const auto& js = e.jacobians();
        auto& re = e0.cell_refelem(e);
        auto rqps = re.integration_points();
        auto dets = e.determinants();
        for (size_t i = 0; i < js.size(); i++)
        {
            double dj = js[i].determinant();
            double dg = dets[i];
            auto relerr = dj/dg - 1;
            
            if ( std::abs(relerr) < 3e-14 )
                throw std::logic_error("Something went wrong with determinants");

            vol_e0_j += dj*rqps[i].weight();
        }
    }
    COMPARE_VALUES_RELATIVE("Entity 0 vol j", vol_e0_j, vol_e0, t1_vals[idx]);

    double vol_e1_j = 0.0;
    for (const auto& e : e1)
    {
        const auto& js = e.jacobians();
        auto& re = e1.cell_refelem(e);
        auto rqps = re.integration_points();
        auto dets = e.determinants();
        for (size_t i = 0; i < js.size(); i++)
        {
            double dj = js[i].determinant();
            double dg = dets[i];
            auto relerr = dj/dg - 1;
            
            if ( std::abs(relerr) < 3e-14 )
                throw std::logic_error("Something went wrong with determinants");

            vol_e1_j += dj*rqps[i].weight();
        }
    }
    COMPARE_VALUES_RELATIVE("Entity 1 vol j", vol_e1_j, vol_e1, t2_vals[idx]);

    /*****************************************/
    std::cout << yellowfg << "Volumes by integrating f(x) = 1 over the domain";
    std::cout << nofg << std::endl;
    double vol_e0_mass = 0.0;
    auto f = [](const point_3d& pt) -> double { return 1; };
    vecxd f_e0 = e0.project(f);
    for (size_t iT = 0; iT < e0.num_cells(); iT++)
    {
        auto& pe = e0.cell(iT);
        auto& re = e0.cell_refelem(pe);
        auto num_bf = re.num_basis_functions();
        matxd mass = e0.mass_matrix(iT);
        vecxd f_pe = f_e0.segment(num_bf*iT, num_bf);
        vol_e0_mass += f_pe.transpose() * mass * f_pe;
    }
    COMPARE_VALUES_RELATIVE("Entity 0 vol mass", vol_e0_mass, vol_e0, t1_vals[idx]);

    double vol_e1_mass = 0.0;
    vecxd f_e1 = e1.project(f);
    for (size_t iT = 0; iT < e1.num_cells(); iT++)
    {
        auto& pe = e1.cell(iT);
        auto& re = e1.cell_refelem(pe);
        auto num_bf = re.num_basis_functions();
        matxd mass = e1.mass_matrix(iT);
        vecxd f_pe = f_e1.segment(num_bf*iT, num_bf);
        vol_e1_mass += f_pe.transpose() * mass * f_pe;
    }
    COMPARE_VALUES_RELATIVE("Entity 1 vol mass", vol_e1_mass, vol_e1, t2_vals[idx]);

    return 0;
}

int
test_normals(int geometric_order, int approximation_order)
{
    make_geometry(0, 0.1);
    gmm::generate( DIMENSION(3) );

    model mod(geometric_order, approximation_order);
    mod.build();

    gmsh::vectorpair pgs;
    gm::getPhysicalGroups(pgs);
    assert(pgs.size() == 1);

    std::cout << cyanfg << "Testing normals (order " << geometric_order;
    std::cout << ")" << reset << std::endl;

    int errors = 0;

    for (auto [dim, pgtag] : pgs)
    {
        std::vector<int> tags;
        gm::getEntitiesForPhysicalGroup(dim, pgtag, tags);
        assert(tags.size() == 1);

        std::vector<int> elemTypes;
        gmm::getElementTypes(elemTypes, dim, tags[0]);
        
        assert(elemTypes.size() == 1);
        if (elemTypes.size() != 1)
            throw std::invalid_argument("Only one element type per entity is allowed");

        for (auto& elemType : elemTypes)
        {
            std::vector<size_t> faceTags, faceNodesTags;
            gmm::getElementsByType(elemType, faceTags, faceNodesTags, tags[0]);

            std::sort(faceTags.begin(), faceTags.end());

            auto& e0 = mod.entity_at(0);

            entity_data_cpu ed;
            e0.populate_entity_data(ed, mod);
            auto ft = e0.face_tags();

            auto bm = mod.get_bnd(7);

            for (size_t iT = 0; iT < e0.num_cells(); iT++)
            {
                for (size_t iF = 0; iF < 4; iF++)
                {
                    auto pf = e0.face(4*iT+iF);
                    auto rf = e0.face_refelem(pf);
                    auto qps = pf.integration_points();
                    auto fbar = pf.barycenter();

                    auto ek = element_key(pf);
                    if ( !std::binary_search(bm.begin(), bm.end(), ek) )
                        continue;

                    size_t num_qp = (geometric_order == 1) ? 1 : qps.size();

                    for (size_t iQp = 0; iQp < num_qp; iQp++)
                    {
                        Eigen::Vector3d n = ed.normals.row((4*iT+iF)*num_qp + iQp);
                        point_3d p = (geometric_order == 1) ? fbar : qps[iQp].point();
                        point_3d v = p - point_3d(0.5, 0.5, 0.5);
                        Eigen::Vector3d nref;
                        nref(0) = v.x(); nref(1) = v.y(); nref(2) = v.z();
                        nref /= nref.norm();

                        if ( n.cross(nref).norm() > 1e-2 )
                        {
                            std::cout << "Wrong normal. Expected: " << greenfg << nref.transpose();
                            std::cout << reset << ", " << "got: " << redfg << n.transpose();
                            std::cout << reset << " diff: " << (nref-n).transpose() << std::endl;
                            errors++;
                        }
                    }
                }
            }
        }
    }

    return errors;
}

int
test_normals_2()
{
    make_geometry(0, 0.1);
    gmm::generate( DIMENSION(3) );
    
    model mod(1, 1);
    mod.build();

    vec3d totflux = vec3d::Zero();
    double tf = 0.0;

    for (const auto& e : mod)
    {
        entity_data_cpu ed;
        e.populate_entity_data(ed, mod);

        for (size_t iT = 0; iT < e.num_cells(); iT++)
        {
            vec3d flux = vec3d::Zero();

            for (size_t iF = 0; iF < 4; iF++)
            {
                auto face_num = 4*iT+iF;
                auto& pf = e.face(face_num);
                vec3d n = ed.normals.row(face_num);
                flux += n * pf.measure();
            }
            tf += flux.norm();
            totflux += flux;
        }
    }

    COMPARE_VALUES_ABSOLUTE("Volume flux", tf, 0, 1e-14);
    COMPARE_VALUES_ABSOLUTE("Volume flux", totflux.norm(), 0, 1e-14);

    return 0;
}

int main(void)
{
    gmsh::initialize();
    gmsh::option::setNumber("General.Terminal", 0);
    gmsh::option::setNumber("Mesh.Algorithm", 1);
    gmsh::option::setNumber("Mesh.Algorithm3D", 1);


    int failed_tests = 0;
/*
    std::cout << Bmagentafg << " *** TESTING: BASIC OPERATIONS ***" << reset << std::endl; 
    for (size_t go = 1; go < 5; go++)
    {
        for (size_t ao = go; ao < 5; ao++)
        {
            failed_tests += test_basics(go, ao);
        }
    }
*/
    //std::cout << Bmagentafg << " *** TESTING: NORMAL COMPUTATION ***" << reset << std::endl;
    //for (size_t i = 1; i < 5; i++)
    //    test_normals(i,i);

    std::cout << Bmagentafg << " *** TESTING: NORMAL COMPUTATION (2) ***" << reset << std::endl;
    test_normals_2();

    return failed_tests;
}
