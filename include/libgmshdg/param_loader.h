/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include "sol/sol.hpp"
#pragma clang diagnostic pop

#include "gmsh_io.h"

enum class field_export_mode {
    NONE,
    NODAL,
    ZONAL
};

enum class time_integrator_type {
    RK4,
    LEAPFROG,
    EULER
};

class parameter_loader_base
{
    bool        m_bnd_sources_active;
    bool        m_ifc_sources_active;
    bool        m_vol_sources_active;
    bool        m_source_changed_state;

protected:
    sol::state lua;

private:
    void init(void);
    bool validate_simulation_params(void) const;

public:
    parameter_loader_base();
    ~parameter_loader_base();

    bool            load_file(const std::string&);

    int             sim_approxorder(void) const;
    int             sim_geomorder(void) const;
    std::string     sim_name(void) const;
    double          sim_dt(void) const;
    size_t          sim_timesteps(void) const;
    std::string     sim_gmshmodel(void) const;
    bool            sim_usegpu(void) const;
    time_integrator_type    sim_timeIntegrator(void) const;
    std::string             sim_timeIntegratorName(void) const;
    size_t          postpro_siloOutputRate(void) const;
    size_t          postpro_cyclePrintRate(void) const;

    std::pair<bool, double>            mesh_scalefactor(void) const;

    void            enable_boundary_sources(bool);
    void            enable_interface_sources(bool);
    void            enable_volume_sources(bool);

    bool            boundary_sources_enabled(void) const;
    bool            interface_sources_enabled(void) const;
    bool            volume_sources_enabled(void) const;

    bool            source_has_changed_state(void) const;
    void            source_was_cleared(void);

    void            call_timestep_callback(size_t);
    void            call_initialization_callback();
    void            call_finalization_callback();


    sol::state&     lua_state(void) { return lua; }
};



