/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <iostream>

#ifdef USE_MPI
#include "common/mpi_helpers.h"
#endif /* USE_MPI */

#include "libgmshdg/reference_element.h"
#include "libgmshdg/physical_element.h"
#include "libgmshdg/connectivity.h"
#include "libgmshdg/entity_data.h"

class model; /* THIS MEANS THAT populate_entity_data() DOES NOT BELONG TO entity*/

using scalar_function = std::function<double(const point_3d&)>;
using vector_function = std::function<vec3d(const point_3d&)>;

class entity
{
    int     dim;
    int     tag;
    int     elemType;
    int     elemType_2D;
    int     g_order;
    int     a_order;
    int     parent_dim;
    int     parent_tag;

    /* Each entity has model offsets, which are offsets relative to the
     * computational unit, and world offsets, which are offsets needed
     * to recover the whole-world vectors of DOFs. */
    size_t                              m_dof_base_model;
    size_t                              m_flux_base_model;
    size_t                              m_index_base_model;
    size_t                              m_entity_number_model;

    size_t                              m_dof_base_world;
    size_t                              m_flux_base_world;
    size_t                              m_index_base_world;
    size_t                              m_entity_number_world;

    entity_ordering                     cur_elem_ordering;

    std::vector<reference_element>      reference_cells;
    std::vector<physical_element>       physical_cells;

    std::vector<reference_element>      reference_faces;
    std::vector<physical_element>       physical_faces;

    std::vector<size_t>                 faceTags;
    std::vector<size_t>                 faceNodesTags;

    using dofs_f2c_t = std::map<size_t, std::vector<size_t>>;
    void populate_lifting_matrices_planar(entity_data_cpu&, const dofs_f2c_t&) const;
    void populate_lifting_matrices_curved(entity_data_cpu&, const dofs_f2c_t&) const;
    void populate_lifting_matrices(entity_data_cpu&) const;

    void populate_differentiation_matrices_planar(entity_data_cpu&) const;
    void populate_differentiation_matrices_curved(entity_data_cpu&) const;
    void populate_differentiation_matrices(entity_data_cpu&) const;

    void populate_jacobians(entity_data_cpu&) const;
    void populate_normals(entity_data_cpu&) const;

public:
    entity() {}// = delete;
    entity(const entity&) = delete;
    entity(entity&&) = default;
    entity& operator=(const entity&) = delete;
    entity& operator=(entity&&) = delete;

    entity(const entity_params& ep);

    double                      measure(void) const;
    matxd                       mass_matrix(size_t) const;

    vecxd                       project(const scalar_function&) const;
    //matxd                       project(const vector_function&) const;
    void                        project(const scalar_function&, vecxd&) const;
    void                        project(const vector_function&, vecxd&, vecxd&, vecxd&) const;
    void                        project(const vector_function&, double *, double *, double *) const;

    matxd                       project_on_face(size_t iF, const vector_function&) const;

    size_t                      num_cells(void) const;
    size_t                      num_cell_orientations(void) const;
    std::vector<size_t>         num_cells_per_orientation(void) const;

    const physical_element&     cell(size_t) const;
    const reference_element&    cell_refelem(size_t) const;
    const reference_element&    cell_refelem(const physical_element&) const;
    size_t                      cell_model_index(size_t) const;
    size_t                      cell_world_index(size_t) const;
    size_t                      cell_local_index_by_gmsh(size_t) const;
    size_t                      cell_model_index_by_gmsh(size_t) const;
    size_t                      cell_world_index_by_gmsh(size_t) const;
    size_t                      cell_local_dof_offset(size_t) const;
    size_t                      cell_model_dof_offset(size_t) const;
    size_t                      cell_world_dof_offset(size_t) const;

    size_t                      num_faces(void) const;
    std::vector<size_t>         face_tags() const;
    const physical_element&     face(size_t) const;
    const reference_element&    face_refelem(size_t) const;
    const reference_element&    face_refelem(const physical_element&) const;

    size_t                      num_dofs(void) const;
    size_t                      num_fluxes(void) const;
    void                        base_model(size_t, size_t, size_t);
    void                        base_world(size_t, size_t, size_t);
    size_t                      dof_base(void) const;
    size_t                      dof_base_world(void) const;
    size_t                      flux_base(void) const;
    size_t                      index_base(void) const;
    size_t                      number(void) const;
    void                        number(size_t);
    
    void                        sort_by_orientation(void);
    void                        sort_by_gmsh(void);

    int                         material_tag(void) const;
    int                         gmsh_tag(void) const;
    int                         gmsh_dim(void) const;
    int                         gmsh_elem_type(void) const;
    entity_ordering             current_elem_ordering(void) const;
    constexpr size_t            num_faces_per_elem(void) const { return 4; }

    /* This probably does not belong here */
    void                        populate_entity_data(entity_data_cpu&, const model&) const;



    std::vector<physical_element>::const_iterator  begin() const { return physical_cells.begin(); }
    std::vector<physical_element>::const_iterator    end() const { return physical_cells.end(); }

#ifdef USE_MPI
    void                        mpi_send(int dst, MPI_Comm comm);
    void                        mpi_recv(int src, MPI_Comm comm);
#endif /* USE_MPI */

};

std::pair<std::vector<point_3d>, std::vector<quadrature_point_3d>>
integrate(const entity& e, int order);
