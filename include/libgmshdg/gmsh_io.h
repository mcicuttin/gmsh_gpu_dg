/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <vector>
#include <array>
#include <gmsh.h>
#include <type_traits>

#ifdef USE_MPI
#include "common/mpi_helpers.h"
#endif /* USE_MPI */

#include "libgmshdg/entity.h"
#include "libgmshdg/physical_element.h"

namespace g     = gmsh;
namespace go    = gmsh::option;
namespace gm    = gmsh::model;
namespace gmm   = gmsh::model::mesh;
namespace gmo   = gmsh::model::occ;
namespace gmg   = gmsh::model::geo;
namespace gv    = gmsh::view;

using gvp_t = gmsh::vectorpair;

/* Macros used to annotate GMSH integer inputs */
#define DIMENSION(x) x

std::string quadrature_name(int);
std::string basis_func_name(int);
std::string basis_grad_name(int);

enum class face_type : int
{
    NONE = 0,
    INTERFACE,
    BOUNDARY,
#ifdef USE_MPI
    INTERPROCESS_BOUNDARY
#endif
};

struct boundary_descriptor
{
    face_type   type;
    int         gmsh_entity;        /* Actual GMSH entity */
#ifdef USE_MPI
    int         parent_entity;      /* Parent GMSH entity in partitioned model */
#endif /* USE_MPI */

    bool operator<(const boundary_descriptor& other) const
    {
        return (type < other.type) or
               ( (type == other.type) and (gmsh_entity < other.gmsh_entity) );
    }

    bool operator==(const boundary_descriptor& other) const
    {
        return (type == other.type) and (gmsh_entity == other.gmsh_entity);
    }

    boundary_descriptor()
        : type(face_type::NONE), gmsh_entity(0)
#ifdef USE_MPI
        , parent_entity(-1)
#endif /* USE_MPI */
    {}

    int material_tag(void) const
    {
#ifdef USE_MPI
        if (parent_entity != -1)
            return parent_entity;
#endif
        return gmsh_entity;
    }
};

#ifdef USE_MPI
struct comm_descriptor
{
    int                     boundary_tag;
    int                     partition_mine;
    int                     partition_other;
    int                     rank_mine;
    int                     rank_other;
    std::vector<size_t>     dof_mapping;
    std::vector<element_key>    fks;
};

inline
std::ostream& operator<<(std::ostream& os, const comm_descriptor& cd)
{
    os << "tag: " << cd.boundary_tag << ", PM: " << cd.partition_mine << ", PO: " << cd.partition_other;
    os << ", RM: " << cd.rank_mine << ", RO: " << cd.rank_other << ", map size: " << cd.dof_mapping.size();
    return os;
}
#endif /* USE_MPI */

#define IMPLIES(a, b) ((not(a)) or (b))
#define IFF(a,b) (IMPLIES(a,b) and IMPLIES(b,a))

class model
{
    /* The objects of the class 'model' are always local to the processor
     * and they contain only the entities pertaining to the specific
     * processor. In the 'model' class however there are additional
     * data structures, which are needed by Rank 0 to keep track of
     * the whole GMSH unpartitioned model. In other words, the 'model'
     * class reflects more a GMSH partition. This mismatch in terminology
     * is due to the fact that MPI was added in a second time.
     * In the code, the term 'model' will thus refer to the GMSH partition,
     * and will be a single computational unit.
     * The collection of the models will be referred as 'world'.
     */

    int                                         geometric_order;
    int                                         approximation_order;

#ifdef USE_MPI
    int                                         num_partitions;
#endif /* USE_MPI */

    /* Map from boundary tag to all the faces of the entity with that tag */
    std::map<int, std::vector<element_key>>     boundary_map;  

    std::vector<entity>                         entities;
    std::vector<boundary_descriptor>            bnd_descriptors;
    element_connectivity<element_key>           conn;

#ifdef USE_MPI
    interprocess_connectivity<element_key>      ipconn;

    /* Map from boundary tag to all the partitions it belongs to */
    std::map<int, std::vector<int>>             comm_map;       

    /* Map from the partition number to the entities belonging to that partition */
    std::map<int, std::vector<int>>             partition_map;
    
    /* In a partitioned model, map from 2D entity tag to parent tag */
    std::map<int, int>                          surface_to_parent_map;

    /* Map from 3D entity tag to partition */
    std::map<int, int>                          partition_inverse_map;

    /* RANK 0 only: keeps track of all the existing entities */
    std::vector<int>                            all_entities_tags;

    /* RANK 0 only: copy of remote entities for postpro */
    std::map<int, std::vector<entity>>          remote_entities;

    std::map<int, comm_descriptor>              ipc_boundary_comm_table; 
#endif /* USE_MPI */

    using entofs_pair = std::pair<size_t, size_t>;
    std::map<size_t, entofs_pair>               etag_to_entity_offset;

    using bfk_t = std::pair<element_key, int>;
    std::vector<bfk_t> get_facekey_tag_pairs(void);

    void    map_boundaries(void);
    void    import_gmsh_entities_rank0(void);
    void    update_connectivity(const entity&, size_t);

    void    populate_from_gmsh_rank0(void);

#ifdef USE_MPI
    bool    is_interprocess_boundary(int);
    void    populate_from_gmsh_rankN(void);
    void    import_gmsh_entities_rankN(void);
    int     my_partition(void) const;
    void    make_partition_to_entities_map(void);
    void    map_interprocess_boundaries(void);
    void    make_comm_descriptors(void);
    std::vector<bfk_t> get_ip_facekey_tag_pairs(void);
#endif /* USE_MPI */

    void    make_boundary_to_faces_map(void);

public:
    model();
    model(int, int);
    model(const char *);
    model(const char *, int, int);
    ~model();

    void build();

    void partition_mesh(int);
    void populate_from_gmsh(void);

#ifdef USE_MPI
    const std::map<int, comm_descriptor>& comm_descriptors(void) const {
        return ipc_boundary_comm_table;
    }

    const std::vector<int>& world_entities_tags(void) const
    {
        return all_entities_tags;
    }

    size_t num_dofs_world() const
    {
        size_t ret = 0;
        for (auto& e : entities)
            ret += e.num_dofs();
        
        for (auto& re : remote_entities)
            for (auto& e : re.second)
                ret += e.num_dofs();

        return ret;
    }

    size_t num_cells_world() const
    {
        size_t ret = 0;
        for (auto& e : entities)
            ret += e.num_cells();
        
        for (auto& re : remote_entities)
            for (auto& e : re.second)
                ret += e.num_cells();
        
        return ret;
    }

    int entity_rank(int tag) const
    {
        if (num_partitions > 1)
            return partition_inverse_map.at(tag)-1;
        
        return 0;
    }

    entity&         entity_world_lookup(int);
    const entity&   entity_world_lookup(int) const;

    const interprocess_connectivity<element_key>& ip_connectivity() const
    {
        return ipconn;
    }
#else
    size_t num_cells_world() const
    {
        return num_cells();
    }

#endif /* USE_MPI */

    const element_connectivity<element_key>& connectivity() const
    {
        return conn;
    }

    const std::vector<boundary_descriptor>& boundary_descriptors() const
    {
        return bnd_descriptors;
    }

    std::vector<element_key> get_bnd(size_t which)
    {
        return boundary_map.at(which);
    }

    size_t  num_dofs() const;
    size_t  num_fluxes() const;
    size_t  num_cells() const;
    size_t  num_faces() const;
    size_t  num_entities() const;

    std::vector<entity>::const_iterator begin() const;
    std::vector<entity>::const_iterator end() const;
    std::vector<entity>::iterator begin();
    std::vector<entity>::iterator end();

    entity&         entity_at(size_t);
    const entity&   entity_at(size_t) const;

    entofs_pair     lookup_tag(size_t tag) const;
};
