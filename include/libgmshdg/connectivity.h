/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include <algorithm>
#include <cassert>
#include <array>
#include <map>
#include <set>

#include "common/point.hpp"
#include "sgr.hpp"

#ifdef USE_MPI
#include "common/mpi_helpers.h"
#endif /* USE_MPI */

/***************************************************************************/
struct neighbour_descriptor
{
    size_t  iE;         /* Entity offset in data structures */
    size_t  iT;         /* Element offset in entity in data structures */
    size_t  via_iF;     /* Element face index (i.e. 0,1,2 for triangles) */

    neighbour_descriptor();
    neighbour_descriptor(size_t, size_t, size_t);
    bool operator<(const neighbour_descriptor&) const;
};

std::ostream& operator<<(std::ostream&, const neighbour_descriptor&);

struct neighbour_descriptor_interprocess
{
    size_t      iE;         /* Entity offset in data structures */
    size_t      iT;         /* Element offset in entity in data structures */
    size_t      via_iF;     /* Element face index (i.e. 0,1,2 for triangles) */

    int         parent_tag; /* Tag of the parent entity */
    int         tag;        /* Tag of the actual entity */
    int         partition;  /* Partition on which the element resides */

    point_3d    bar;        /* Barycenter, for materials. */

    neighbour_descriptor_interprocess();
    bool operator<(const neighbour_descriptor_interprocess&) const;

};

std::ostream& operator<<(std::ostream&, const neighbour_descriptor_interprocess&);

/***************************************************************************/
struct cell_descriptor
{
    size_t  iE;
    size_t  iT;

    cell_descriptor();
    cell_descriptor(size_t, size_t);
    bool operator<(const cell_descriptor&) const;
};

std::ostream& operator<<(std::ostream&, const cell_descriptor&);

/***************************************************************************/
template<typename NT>
class neighbour_pair
{
    NT      n_first;
    NT      n_second;
    size_t  n_used;

public:
    neighbour_pair()
        : n_used(0)
    {}

    void
    add_neighbour(const NT& n)
    {
        assert(n_used == 0 or n_used == 1);
        if (n_used == 0)
        {
            n_first = n;
            n_used = 1;
            return;
        }
        
        if (n_used == 1)
        {
            n_second = n;
            n_used = 2;
            return;
        }

        throw std::out_of_range("Pair has already two neighbours");
    }

    NT first() const
    {
        assert(n_used == 1 or n_used == 2);
        if (n_used < 1)
            throw std::out_of_range("Pair does not have n_first");

        return n_first;
    }

    NT second() const
    {
        assert(n_used == 2);
        if (n_used < 2)
            throw std::out_of_range("Pair does not have n_second");

        return n_second;
    }

    size_t num_neighbours() const
    {
        assert(n_used < 3);
        return n_used;
    }
};

/***************************************************************************/
template<typename FaceKey>
class element_connectivity
{
public:
    using ConnData  = std::pair<FaceKey, neighbour_descriptor>;
    using NeighPair = neighbour_pair<neighbour_descriptor>;

private:
    std::map<FaceKey, NeighPair>                    face_cell_adj;
    std::map<cell_descriptor, std::set<ConnData>>   cell_cell_adj;

public:
    element_connectivity()
    {}

    void
    connect(const FaceKey& fk, const neighbour_descriptor& nd)
    {
        /* Insert the information that one the two neighbours around
         * face `e' is `nd'. */
        auto& nr = face_cell_adj[fk];
        nr.add_neighbour(nd);

        /* If we've got both neighbours around `e`, add a connection
         * between them in the cell-cell adjacency list */
        if ( nr.num_neighbours() == 2 )
        {
            auto fst = nr.first();
            auto snd = nr.second();
            cell_cell_adj[{fst.iE, fst.iT}].insert({fk, snd});
            cell_cell_adj[{snd.iE, snd.iT}].insert({fk, fst});
        }
    }

    /* Find the neighbour of the element (iE,iT) via face fk */
    std::pair<neighbour_descriptor, bool>
    neighbour_via(size_t iE, size_t iT, const FaceKey& fk) const
    {
        auto nr = face_cell_adj.at(fk);
        
        if ( nr.num_neighbours() == 1 )
        {
#ifndef NDEBUG
            auto fst = nr.first();
            assert(fst.iE == iE);
            assert(fst.iT == iT);
#endif
            return std::make_pair(neighbour_descriptor(), false);
        }

        if ( nr.num_neighbours() == 2 )
        {
            auto fst = nr.first();
            auto snd = nr.second();

            assert( (fst.iE == iE and fst.iT == iT) or
                    (snd.iE == iE and snd.iT == iT)
                  );

            if ( fst.iE == iE and fst.iT == iT)
                return std::make_pair(snd, true);

            return std::make_pair(fst, true);
        }

        assert(true && "Shouldn't have arrived here");
        return std::make_pair(neighbour_descriptor(), false); /* Silence the compiler */
    }

    size_t
    num_neighbours(const FaceKey& fk)
    {
        size_t nn = face_cell_adj.at(fk).num_neighbours();
        assert(nn == 1 or nn == 2);
        return nn;
    }

    std::set<ConnData>
    neighbours(size_t iE, size_t iT) const
    {
        return cell_cell_adj.at({iE, iT});
    }

    std::set<ConnData>
    neighbours(const cell_descriptor& cd) const
    {
        return cell_cell_adj.at(cd);
    }

    std::set<cell_descriptor>
    neighbours_as_cell_descriptors(const cell_descriptor& cd) const
    {
        auto filter = [](const ConnData& connd) {
            cell_descriptor ret;
            ret.iE = connd.second.iE;
            ret.iT = connd.second.iT;
            return ret;
        };

        auto neighs = cell_cell_adj.at(cd);
        std::set<cell_descriptor> ret;
        std::transform(neighs.begin(), neighs.end(),
                       std::inserter(ret, ret.begin()), filter);
        return ret; 
    }

#if (__cplusplus >= 201703L) /* This is a quick hack for nvcc */
    bool
    are_connected(const cell_descriptor& a, const cell_descriptor& b) const
    {
        auto neighs = neighbours(a);
        for (const auto& [fk, nd] : neighs)
            if (nd.iE == b.iE and nd.iT == b.iT)
                return true;

        return false;
    }
#endif

    std::set<cell_descriptor>
    element_keys() const
    {
        using PT = std::pair<cell_descriptor, std::set<ConnData>>;
        auto get_node = [](const PT& adjpair) {
            return adjpair.first;
        };

        auto cc_begin = cell_cell_adj.begin();
        auto cc_end = cell_cell_adj.end();
        std::set<cell_descriptor> ret;
        std::transform(cc_begin, cc_end, std::inserter(ret, ret.begin()), get_node);
        return ret;
    }

    void clear(void)
    {
        face_cell_adj.clear();
    }
};




/***************************************************************************/
#ifdef USE_MPI
template<typename FaceKey>
class interprocess_connectivity
{
public:
    using ConnData  = std::pair<FaceKey, neighbour_descriptor_interprocess>;
    using NeighPair = neighbour_pair<neighbour_descriptor_interprocess>;

private:
    std::map<FaceKey, NeighPair>                    face_cell_adj;

public:
    interprocess_connectivity()
    {}

    void
    connect(const FaceKey& fk, const neighbour_descriptor_interprocess& nd)
    {
        /* Insert the information that one the two neighbours around
         * face `e' is `nd'. */
        auto& nr = face_cell_adj[fk];
        nr.add_neighbour(nd);
    }

    /* Find the neighbour of the element (iE,iT) via face fk */
    std::pair<neighbour_descriptor_interprocess, bool>
    neighbour_via(size_t iE, size_t iT, const FaceKey& fk) const
    {
        auto itor = face_cell_adj.find(fk);
        if (itor == face_cell_adj.end())
            return std::make_pair(neighbour_descriptor_interprocess(), false);

        auto nr = (*itor).second;
        assert( nr.num_neighbours() == 2 );

        auto fst = nr.first();
        auto snd = nr.second();

        assert( (fst.iE == iE and fst.iT == iT) or
                (snd.iE == iE and snd.iT == iT)
              );

        if ( fst.iE == iE and fst.iT == iT)
            return std::make_pair(snd, true);

        return std::make_pair(fst, true);
    }

    void clear(void)
    {
        face_cell_adj.clear();
    }

    void dump(void)
    {
        for (auto& [key, pair] : face_cell_adj)
        {
            std::cout << key << ": " << pair.first() << " - " << pair.second() << std::endl;
        }
    }

    void mpi_bcast(int root, MPI_Comm comm)
    {
        priv_MPI_Bcast(face_cell_adj, root, comm);
    }
};

#endif /* USE_MPI */

