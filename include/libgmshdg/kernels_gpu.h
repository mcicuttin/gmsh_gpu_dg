/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once
#include "common/types.h"
#include "libgmshdg/entity_data.h"

/* Compile-time constants configuring sizes for the kernels. */

template<size_t K>
struct kernel_gpu_sizes;

template<>
struct kernel_gpu_sizes<1>
{
    static const size_t num_bf              = num_dofs_3D(1); //4
    static const size_t num_fluxes          = num_dofs_2D(1); //3

    static const size_t deriv_threads       = 128;
    static const size_t lifting_threads     = 128;

    static const size_t cells_per_dblock    = 32;
    static const size_t dofs_per_dblock     = num_bf * cells_per_dblock;
    static const size_t dblock_size         = 128;
    static const size_t parallel_dblocks    = 1;
};

template<>
struct kernel_gpu_sizes<2>
{
    static const size_t num_bf              = num_dofs_3D(2); //10
    static const size_t num_fluxes          = num_dofs_2D(2); //6

    static const size_t deriv_threads       = 128;
    static const size_t lifting_threads     = 128;

    static const size_t cells_per_dblock    = 12;
    static const size_t dofs_per_dblock     = num_bf * cells_per_dblock;
    static const size_t dblock_size         = 128;
    static const size_t parallel_dblocks    = 1;
};

template<>
struct kernel_gpu_sizes<3>
{
    static const size_t num_bf              = num_dofs_3D(3); //20
    static const size_t num_fluxes          = num_dofs_2D(3); //10

    static const size_t deriv_threads       = 128;
    static const size_t lifting_threads     = 128;

    static const size_t cells_per_dblock    = 6;
    static const size_t dofs_per_dblock     = num_bf * cells_per_dblock;
    static const size_t dblock_size         = 128;
    static const size_t parallel_dblocks    = 1;
};

template<>
struct kernel_gpu_sizes<4>
{
    static const size_t num_bf              = num_dofs_3D(4); //35
    static const size_t num_fluxes          = num_dofs_2D(4);

    static const size_t deriv_threads       = 512;
    static const size_t lifting_threads     = 128;

    static const size_t cells_per_dblock    = 3;
    static const size_t dofs_per_dblock     = num_bf * cells_per_dblock;
    static const size_t dblock_size         = 128;
    static const size_t parallel_dblocks    = 4;
};

template<>
struct kernel_gpu_sizes<5>
{
    static const size_t num_bf              = num_dofs_3D(5);
    static const size_t num_fluxes          = num_dofs_2D(5);

    static const size_t deriv_threads       = 1024;
    static const size_t lifting_threads     = 128;

        static const size_t cells_per_dblock    = 32;
    static const size_t dofs_per_dblock     = num_bf * cells_per_dblock;
    static const size_t dblock_size         = 128;
    static const size_t parallel_dblocks    = 4;
};

template<>
struct kernel_gpu_sizes<6>
{
    static const size_t num_bf              = num_dofs_3D(6);
    static const size_t num_fluxes          = num_dofs_2D(6);

    static const size_t deriv_threads       = 1024;
    static const size_t lifting_threads     = 128;

        static const size_t cells_per_dblock    = 32;
    static const size_t dofs_per_dblock     = num_bf * cells_per_dblock;
    static const size_t dblock_size         = 128;
    static const size_t parallel_dblocks    = 4;
};

struct kernel_gpu_sizes_runtime
{
    size_t      num_bf;
    size_t      cells_per_dblock;
    size_t      dblock_size;
};

template<size_t N>
kernel_gpu_sizes_runtime get_kernel_gpu_sizes()
{
    kernel_gpu_sizes_runtime ret;
    
    using KS = kernel_gpu_sizes<N>;

    ret.num_bf              = KS::num_bf;
    ret.cells_per_dblock    = KS::cells_per_dblock;
    ret.dblock_size         = KS::dblock_size;


    return ret;
};

inline kernel_gpu_sizes_runtime
get_kernel_gpu_sizes(int approx_order)
{
    switch (approx_order)
    {
        case 1:     return get_kernel_gpu_sizes<1>();
        case 2:     return get_kernel_gpu_sizes<2>();
        case 3:     return get_kernel_gpu_sizes<3>();
        case 4:     return get_kernel_gpu_sizes<4>();
    }

    throw std::invalid_argument("Can't retrieve info for this element");
}

size_t gpu_dblocks(const entity_data_cpu&);
size_t gpu_dblocks_dofs(const entity_data_cpu&);

#define TO_GPU      true
#define FROM_GPU    false
void reshape_dofs(const entity_data_cpu&, const entity_data_gpu&, const vecxd&, vecxd&, bool);


void
gpu_compute_field_derivatives(const entity_data_gpu& edg, const double *F,
    double *dF_dx, double* dF_dy, double* dF_dz, double alpha, gpuStream_t stream = 0);

void gpu_compute_flux_lifting(entity_data_gpu&, const double *, double *,
    gpuStream_t stream = 0);

void gpu_curl(double *, const double *, const double *, size_t,
    gpuStream_t stream = 0);

void gpu_curl(double *, const double *, const double *, const double *,
    size_t, gpuStream_t stream = 0);

void
gpu_compute_euler_update(double *out, const double *y, const double *k,
    const double *reaction, const double *material, size_t num_dofs, double dt,
    gpuStream_t stream = 0);

void
gpu_compute_euler_update(double *out, const double *y, const double *k,
    const double *material, size_t num_dofs, double dt,
    gpuStream_t stream = 0);

void
gpu_compute_rk4_weighted_sum(double *out, const double *in, const double *k1,
    const double *k2, const double *k3, const double *k4,
    const double *material, size_t num_dofs, double dt,
    gpuStream_t stream = 0);

void
gpu_compute_rk4_weighted_sum(double *out, const double *in, const double *k1,
    const double *k2, const double *k3, const double *k4, const double *reaction,
    const double *material, size_t num_dofs, double dt,
    gpuStream_t stream = 0);

void
gpu_compute_leapfrog_update(double *next, const double *curr, const double *soe,
    const double *tmp, const double *ie, size_t num_dofs, double dt,
    gpuStream_t stream = 0);

void
gpu_compute_leapfrog_update(double *next, const double *curr,
    const double *tmp, const double *im, size_t num_dofs, double dt,
    gpuStream_t stream = 0);

