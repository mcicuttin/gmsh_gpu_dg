/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#include "common/types.h"

template<size_t AK>
struct kernel_cpu_sizes
{
    static const size_t num_bf = num_dofs_3D(AK);
    static const size_t num_fluxes = num_dofs_2D(AK);
    static const size_t num_faces = 4;
    static const size_t num_all_fluxes = num_fluxes * num_faces;
};

template<size_t AK>
void compute_field_derivatives_kernel_planar(const entity_data_cpu& ed,
    const vecxd& f, vecxd& df_dx, vecxd& df_dy, vecxd& df_dz)
{
    /* Flop count: 21*ed.num_bf*ed.num_bf*num_total_elems */
    /* The elements must be ordered by orientation -> entity::sort_by_orientation() */

    assert(ed.g_order == 1);

    using KS = kernel_cpu_sizes<AK>;
    assert(ed.num_bf == KS::num_bf);

    size_t orient_elem_base = 0;
    for (size_t iO = 0; iO < ed.num_orientations; iO++)
    {
        size_t num_elems = ed.num_elems[iO];
        size_t orient_base = orient_elem_base * ed.num_bf;

        #pragma omp parallel for //num_threads(2)
        for (size_t iT = 0; iT < num_elems; iT++)
        {
            size_t ent_iT = orient_elem_base + iT;
            size_t dofofs = ed.dof_base + orient_base + iT*ed.num_bf;
            
            for (size_t odof = 0; odof < KS::num_bf; odof++)
            {
                double acc_df_dx = 0.0;
                double acc_df_dy = 0.0;
                double acc_df_dz = 0.0;

                for (size_t idof = 0; idof < KS::num_bf; idof++)
                {
                    double F = f(dofofs+idof);
                    auto dm_row = iO*KS::num_bf + odof;
                    auto dm_col0 = 0*KS::num_bf + idof;
                    auto dm_col1 = 1*KS::num_bf + idof;
                    auto dm_col2 = 2*KS::num_bf + idof;
                    double v0 = ed.differentiation_matrices(dm_row, dm_col0) * F;
                    double v1 = ed.differentiation_matrices(dm_row, dm_col1) * F;
                    double v2 = ed.differentiation_matrices(dm_row, dm_col2) * F;
                    
                    acc_df_dx += ed.jacobians(3*ent_iT + 0, 0) * v0;
                    acc_df_dx += ed.jacobians(3*ent_iT + 0, 1) * v1;
                    acc_df_dx += ed.jacobians(3*ent_iT + 0, 2) * v2;

                    acc_df_dy += ed.jacobians(3*ent_iT + 1, 0) * v0;
                    acc_df_dy += ed.jacobians(3*ent_iT + 1, 1) * v1;
                    acc_df_dy += ed.jacobians(3*ent_iT + 1, 2) * v2;

                    acc_df_dz += ed.jacobians(3*ent_iT + 2, 0) * v0;
                    acc_df_dz += ed.jacobians(3*ent_iT + 2, 1) * v1;
                    acc_df_dz += ed.jacobians(3*ent_iT + 2, 2) * v2;
                }

                df_dx(dofofs+odof) = acc_df_dx;
                df_dy(dofofs+odof) = acc_df_dy;
                df_dz(dofofs+odof) = acc_df_dz;
            }
        }

        orient_elem_base += num_elems;
    }
}

template<size_t AK>
void compute_field_derivatives_kernel_curved(const entity_data_cpu& ed,
    const vecxd& f, vecxd& df_dx, vecxd& df_dy, vecxd& df_dz)
{
    /* Flop count: ((21*ed.num_bf+6)*ed.num_bf*ed.num_qp + 3*(2*ed.num_bf-1)*ed.num_bf)*num_elements */
    /* The elements must be ordered by orientation -> entity::sort_by_orientation() */

    assert(ed.g_order > 1);

    using KS = kernel_cpu_sizes<AK>;
    assert(ed.num_bf == KS::num_bf);

    size_t orient_elem_base = 0;
    for (size_t iO = 0; iO < ed.num_orientations; iO++)
    {
        size_t num_elems = ed.num_elems[iO];
        size_t orient_base = orient_elem_base * ed.num_bf;


        #pragma omp parallel for// num_threads(2)
        for (size_t iT = 0; iT < num_elems; iT++)
        {
            //printf( "Thread %d works with idx %lu\n", omp_get_thread_num(), iT);
            size_t ent_iT = orient_elem_base + iT;
            size_t dofofs = ed.dof_base + orient_base + iT*ed.num_bf;

            df_dx.segment(dofofs, ed.num_bf) = vecxd::Zero(ed.num_bf);
            df_dy.segment(dofofs, ed.num_bf) = vecxd::Zero(ed.num_bf);
            df_dz.segment(dofofs, ed.num_bf) = vecxd::Zero(ed.num_bf);
            
            for (size_t iQp = 0; iQp < ed.num_qp; iQp++)
            {
                auto det = ed.cell_determinants(ent_iT*ed.num_qp + iQp);
                for (size_t odof = 0; odof < KS::num_bf; odof++)
                {
                    double acc_df_dx = 0.0;
                    double acc_df_dy = 0.0;
                    double acc_df_dz = 0.0;

                    for (size_t idof = 0; idof < KS::num_bf; idof++)
                    {
                        double F = f(dofofs+idof);
                        auto dm_row = iO*KS::num_bf*ed.num_qp + KS::num_bf*iQp + odof;
                        auto dm_col0 = 0*KS::num_bf + idof;
                        auto dm_col1 = 1*KS::num_bf + idof;
                        auto dm_col2 = 2*KS::num_bf + idof;
                        double v0 = ed.differentiation_matrices(dm_row, dm_col0) * F;
                        double v1 = ed.differentiation_matrices(dm_row, dm_col1) * F;
                        double v2 = ed.differentiation_matrices(dm_row, dm_col2) * F;
                        
                        auto jbase = 3*ent_iT*ed.num_qp + 3*iQp;
                        acc_df_dx += ed.jacobians(jbase + 0, 0) * v0;
                        acc_df_dx += ed.jacobians(jbase + 0, 1) * v1;
                        acc_df_dx += ed.jacobians(jbase + 0, 2) * v2;

                        acc_df_dy += ed.jacobians(jbase + 1, 0) * v0;
                        acc_df_dy += ed.jacobians(jbase + 1, 1) * v1;
                        acc_df_dy += ed.jacobians(jbase + 1, 2) * v2;

                        acc_df_dz += ed.jacobians(jbase + 2, 0) * v0;
                        acc_df_dz += ed.jacobians(jbase + 2, 1) * v1;
                        acc_df_dz += ed.jacobians(jbase + 2, 2) * v2;
                    }

                    df_dx(dofofs+odof) += det*acc_df_dx;
                    df_dy(dofofs+odof) += det*acc_df_dy;
                    df_dz(dofofs+odof) += det*acc_df_dz;
                }
            }
            
            df_dx.segment(dofofs, ed.num_bf) =
                ed.invmass_matrices.block(ent_iT*ed.num_bf,0,ed.num_bf,ed.num_bf)*df_dx.segment(dofofs, ed.num_bf);

            df_dy.segment(dofofs, ed.num_bf) =
                ed.invmass_matrices.block(ent_iT*ed.num_bf,0,ed.num_bf,ed.num_bf)*df_dy.segment(dofofs, ed.num_bf);

            df_dz.segment(dofofs, ed.num_bf) =
                ed.invmass_matrices.block(ent_iT*ed.num_bf,0,ed.num_bf,ed.num_bf)*df_dz.segment(dofofs, ed.num_bf);
        }

        orient_elem_base += num_elems;
    }
}


void compute_field_derivatives(const entity_data_cpu& ed,
    const vecxd& f, vecxd& df_dx, vecxd& df_dy, vecxd& df_dz);


template<size_t AK>
void compute_lifting_kernel_planar(const entity_data_cpu& ed,
    const vecxd& flux, vecxd& field)
{
    /* Flop count: 21*ed.num_bf*ed.num_bf*num_total_elems */
    /* The elements must be ordered by orientation -> entity::sort_by_orientation() */

    assert(ed.g_order == 1);

    using KS = kernel_cpu_sizes<AK>;
    assert(ed.num_bf == KS::num_bf);
    assert(ed.num_fluxes == KS::num_fluxes);

    size_t orient_elem_base = 0;
    for (size_t iO = 0; iO < ed.num_orientations; iO++)
    {
        size_t num_elems = ed.num_elems[iO];
        size_t dof_base = ed.dof_base + orient_elem_base * ed.num_bf;
        size_t flux_base = ed.flux_base + orient_elem_base * 4*ed.num_fluxes;

        #pragma omp parallel for
        for (size_t iT = 0; iT < num_elems; iT++)
        {
            size_t dof_ofs = dof_base + iT*ed.num_bf;
            size_t flux_ofs = flux_base + 4*iT*KS::num_fluxes;
            auto inv_det = 1./ed.cell_determinants[orient_elem_base+iT];

            field.segment<KS::num_bf>(dof_ofs) += (inv_det) * ed.lifting_matrices.block<KS::num_bf, 4*KS::num_fluxes>(iO*KS::num_bf, 0) * flux.segment<4*KS::num_fluxes>(flux_ofs);
        }

        orient_elem_base += num_elems;
    }
}

template<size_t AK>
void compute_lifting_kernel_curved(const entity_data_cpu&,
    const vecxd&, vecxd&)
{}

void compute_flux_lifting(const entity_data_cpu& ed,
    const vecxd& flux, vecxd& field);
