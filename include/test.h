/* This is GMSH/DG, a GPU-Accelerated Nodal Discontinuous Galerkin
 * solver for Conservation Laws.
 *
 * Copyright (C) 2020-2022 Matteo Cicuttin - University of Liège
 * 
 * This code is released under GNU AGPLv3 license, see LICENSE.txt for details.
 */

#pragma once

#define COMPARE_VALUES_ABSOLUTE(name, val, ref, tol)                    \
    std::cout << "Test: " << name << "...";                             \
    if ( double real_tol = std::abs(val - ref); real_tol < tol )        \
    { std::cout << greenfg << "PASS" << nofg << " (val = " << val;      \
      std::cout << ", ref = " << ref << ", tol = " << tol;              \
      std::cout << ", real_tol = " << real_tol << std::endl; }          \
    else                                                                \
    { std::cout << redfg << "FAIL" << nofg << " (val = " << val;        \
      std::cout << ", ref = " << ref << ", tol = " << tol;              \
      std::cout << ", real_tol = " << real_tol << std::endl; return 1; }

#define COMPARE_VALUES_RELATIVE(name, val, ref, tol)                                \
    std::cout << "Test: " << name << "...";                                         \
    if ( double real_tol = (std::abs(val - ref)/std::abs(ref)); real_tol < tol )    \
    { std::cout << greenfg << "PASS" << nofg << " (val = " << val;                  \
      std::cout << ", ref = " << ref << ", tol = " << tol;                          \
      std::cout << ", real_tol = " << real_tol << std::endl; }                      \
    else                                                                            \
    { std::cout << redfg << "FAIL" << nofg << " (val = " << val;        \
      std::cout << ", ref = " << ref << ", tol = " << tol;              \
      std::cout << ", real_tol = " << real_tol << std::endl; return 1; }

#define SPHERE_RADIUS 0.2

int test_basics(int, int);
int test_normals(int, int);
int test_mass_convergence(int, int);
int test_differentiation_convergence(int, int);
int test_lifting(int, int);
