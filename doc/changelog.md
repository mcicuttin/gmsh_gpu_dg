# Changelog

## Version v0.1 (09/05/2021)
Initial version.

Available features:
- Boundary sources and interface sources both on CPU and GPU. In GPU computations, sources are computed asynchronously on the CPU and then uploaded, and this can be suboptimal if not problematic (see `lua_api.md` for the details). The solver implements `"plane_wave_E"` and `"surface_current"` as boundary sources. It implements `"surface_current"` as interface sources.

Unavailable features:
- Geometric order is limited to 1. Approximation order is limited to 6.
- Volumetric sources are not available yet.
- In the Maxwell solver, the RK4 time integrator limits the practical values of the conducibility `sigma`. If the absolute value of `1 - sigma*sim.dt/(epsilon*const.eps0)` is greater than 1, the computation fails. This will be solved by leapfrog time integration in a subsequent version.
- Repetitive sources on GPU.

## Version v0.2 (17/05/2021)
- Fixed performance issue in the evaluation of boundary/interface sources (4463681c).
- Compressed source data for upload on GPU (27bb4c44).
- Added HIP support via Hipify (e6f5a020).
- Switched back to C++17 due to various problems on Eigen and Mac OS X.
- Use system GMSH if installed and only after look at the hardcoded path (a55af8ab).
- Improved export to SILO. E, H, and J fields are now exportable as nodal or zonal variables. It is also possible to disable the export of a specific field, see `lua_api.md` (8a831181).
- Leapfrog integration on CPU & GPU (e3d95022).

## Version v0.3 (30/09/2021)
- Volumetric sources on CPU & GPU (2ed2e9e7).
- Fixed issues that prevented compilation on CECI clusters.
- Minor performance optimizations
- Line integration (preliminary code for geometric diode simulation)
- MPI support for the CPU solver
- Validation API on Lua configuration

