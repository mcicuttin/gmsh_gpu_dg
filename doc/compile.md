# Compile instructions

## Supported systems
This code is developed under Linux and Mac OS. GPU support is available
only under Linux. A Windows port should be possible, but for the moment
is not in the plans.

## Code dependencies
The code is designed to keep the number of dependencies at the minimum, however it
is not totally self-contained. In order to run the code, you need to provide the
following dependencies:

- **GMSH**: Debian and Homebrew packages should work fine, however for completely
  hassle-free experience it is suggested either to install GMSH from source
  and put it in `/opt/uliege/gmsh` or grab a binary build [here](http://gmsh.info/)
  and decompress it in `/opt/uliege/gmsh`.
- **Eigen**: again, Debian and Homebrew packages should work fine. If not, install the
  latest version of Eigen from their site and configure `gmsh_gpu_dg` with
  
```
ccmake .. -DEigen3_DIR=/your/eigen/install/path/eigen/cmake/
```

- **Lua**: All the simulation configurations are written in Lua.
- **SILO/VisIt**: SILO is a scientific database used to store simulation results,
  VisIt is a scientific data visualization tool
- **CUDA**: if you want GPU support
- **OpenMPI**: if you want parallel processing

### Package installation on Debian

```
apt install libeigen3-dev libsilo-dev libsiloh5-0 liblua5.3-dev
```

### Package installation on Mac OS
```
brew install eigen lua
brew tap datafl4sh/code
brew install datafl4sh/code/silo
```

## Compilation
The code uses CMake to handle the builds. The whole procedure should be

```
git clone --recursive https://gitlab.onelab.info/mcicuttin/gmsh_gpu_dg.git
cd gmsh_gpu_dg
mkdir build
cd build
ccmake ..
make
```

If you don't compile tests, you will get an executable called `maxwell_solver`.
It takes a single parameter, the path of the Lua configuration script (see in
`share` for some examples).

To visualize the results you need VisIt from [here](https://wci.llnl.gov/simulation/computer-codes/visit).
